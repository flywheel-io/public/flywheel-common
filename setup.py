import os

from setuptools import setup, find_packages


with open('requirements.txt') as f:
    requirements = f.read().splitlines()

VERSION = os.getenv('CI_COMMIT_TAG', '0.0.0.dev1')

setup(name='flywheel-common',
      version=VERSION,
      description='Collection of toolkits to be used across the Flywheel platform',
      url='https://github.com/flywheel-io/flywheel-common',
      author='Flywheel',
      author_email='support@flywheel.io',
      license='MIT',
      packages=find_packages(),
      install_requires=requirements,
      extras_require={
          'gevent': ['gevent_inotifyx==0.2']
      },
      zip_safe=False)
