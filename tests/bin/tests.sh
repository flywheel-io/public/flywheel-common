#!/usr/bin/env sh
set -eu
unset CDPATH
cd "$( dirname "$0" )/../.."


USAGE="
Usage:
    $0 [OPTION...] [[--] PYTEST_ARGS...]

Runs all tests (unit, integ and linting) if no options are provided.

Options:
    -h, --help           Print this help and exit

    -s, --shell          Enter shell instead of running tests

    PYTEST_ARGS          Arguments passed to py.test
"


main() {
    export PYTHONDONTWRITEBYTECODE=1
    local RUN_SHELL=false

    while [ $# -gt 0 ]; do
        case "$1" in
            -h|--help)
                log "$USAGE"
                exit 0
                ;;
            -s|--shell)
                RUN_SHELL=true
                ;;
            --)
                shift
                break
                ;;
            *)
                break
                ;;
        esac
        shift
    done

        if [ $RUN_SHELL = true ]; then
            log "INFO: Entering test shell ..."
            sh
            exit
        fi

        log "INFO: Running unit tests ..."
        python -m pytest --exitfirst tests/ "$@"
}

log() {
    printf "\n%s\n" "$@" >&2
}

main "$@"
