#!/usr/bin/env sh

set -eu
unset CDPATH
cd "$( dirname "$0" )/../.."


USAGE="
Usage:
    $0 [OPTION...] [[--] TEST_ARGS...]

Build flywheel/common image and run tests in a Docker container.

Options:
    -h, --help          Print this help and exit

    -B, --no-build      Skip rebuilding default Docker image

    TEST_ARGS           Arguments passed to tests.sh

"

DOCKER_IMAGE="flywheel/common:testing"

main() {
    local BUILD="true"

    while [ $# -gt 0 ]; do
        case "$1" in
            -h|--help)
                log "$USAGE"
                exit 0
                ;;
            -B|--no-build)
                BUILD="false"
                ;;
            --)
                shift
                break
                ;;
            *)
                break
                ;;
        esac
        shift
    done

    if [ "${BUILD}" = "true" ]; then
        docker build -t "${DOCKER_IMAGE}" .
    fi

    docker run -it --rm \
        --name common-test \
        --volume $(pwd):/src/common \
        --workdir /src/common \
        "${DOCKER_IMAGE}" \
        tests/bin/tests.sh "$@"
}


log() {
    printf "\n%s\n" "$@" >&2
}


main "$@"
