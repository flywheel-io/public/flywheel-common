import logging
import sys
import flylogging
import time
import os


CURRENT_DIR = os.path.dirname(__file__)

flylogging.init_flywheel_logging('/' + os.path.join(CURRENT_DIR, 'logging_config.yml'))
log = logging.getLogger('Harsha')
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.DEBUG)
log.addHandler(out_hdlr)
root = logging.getLogger()
while True:
    root.debug('root: debug')
    root.info('root: info')
    root.warn('root: warn')
    root.error('root: error')
    root.critical('root: critical')
    time.sleep(2)
    log.debug('debug')
    log.info('info')
    log.warn('warn')
    log.error('error')
    log.critical('critical')
    time.sleep(2)
